# kde-flatpaks

Random flatpaks in yaml format.

Add Flathub & KDE SDK:
```
flatpak --user remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak --user install flathub runtime/org.kde.Sdk/x86_64/5.15
```

Example to build dragonplayer:
```
name=dragonplayer
flatpak-builder --force-clean --delete-build-dirs --arch=x86_64 --ccache --sandbox --user --install-deps-from=flathub --repo=staging-repo/ app org.kde.${name}.yml
flatpak build-bundle staging-repo/ org.kde.${name}.flatpak org.kde.${name}
flatpak --user install -y org.kde.${name}.flatpak
flatpak run org.kde.${name}
```
